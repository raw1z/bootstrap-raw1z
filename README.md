# Bootstrap raw1z

Get the ansible scripts used to setup a complete environment.

1.  Install required tools

    ```
    ./{env}/01_install_requirements.sh
    ```

2.  Setup 1password

    ```
    ./02_setup_1password.sh
    ```

3.  Setup ansible vault

    ```
    ./03_setup_ansible_vault.sh
    ```

4.  Clone ansible repository, init virtual env and install requirements

    ```
    ./04_get_ansible_raw1z.sh
    ```

5.  Bootstrap the environment

    ```
    ./05_bootstrap.sh
    ```

We assume an arch linux based distribution.
