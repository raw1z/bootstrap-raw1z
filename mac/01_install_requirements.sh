#!/bin/bash

set -ex

brew install jq
brew install --cask 1password-cli
