#!/bin/bash

set -ex

# upgrade system first
sudo pacman -Syu --noconfirm

# install requirements
sudo pacman -S --needed --noconfirm base-devel jq git python

# install paru
git clone https://aur.archlinux.org/paru.git ~/paru
pushd ~/paru
makepkg -si
popd

# install 1password-cli
gpg --keyserver hkps://keyserver.ubuntu.com --receive-keys 3FEF9748469ADBE15DA7CA80AC2D62742012EA22
paru -S --noconfirm 1password-cli

