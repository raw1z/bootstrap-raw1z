#!/bin/bash

set -e

cd ~/workspace/github/ansible-raw1z
source env/bin/activate
invoke run
