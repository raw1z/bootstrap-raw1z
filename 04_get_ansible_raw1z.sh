#!/bin/bash

set -e

# clone folder
url=$(op read "op://Personal/gitlab-perso/clone-url")
folder=~/workspace/github/ansible-raw1z
git clone $url $folder

# activate virtual env
cd $folder
python3 -m venv env
source env/bin/activate

# install requirements
pip install --upgrade pip
pip install -r requirements.txt
ansible-galaxy collection install community.general
ansible-galaxy install -r requirements.yml

